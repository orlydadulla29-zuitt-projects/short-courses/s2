let studentOneName = "John"
let studentOneEmail = "john@mail.com"
let studentOneGrades = [89, 84, 78, 88]

let studentTwoName = "Joe"
let studentTwoEmail = "joe@mail.com"
let studentTwoGrades = [78, 82, 79, 85]

let studentThreeName = "Jane"
let studentThreeEmail = "jane@mail.com"
let studentThreeGrades = [87, 89, 91, 93]

let studentFourName = "Jessie"
let studentFourEmail = "jessie@mail.com"
let studentFourGrades = [91, 89, 92, 93]


//Translate the other students from our boilerplate code into their own respective objects. Done

//Define a method for EACH student object that will compute for their grade average (total of grades divided by 4) Done

//Define a method for all student objects named willPass() that returns a Boolean value indicating if student will pass or fail. For a student to pass, their ave. grade must be greater than or equal to 85. Done

//Define a method for all student objects named willPassWithHonors() that returns true if ave. grade is greater than or equal to 90, false if >= 85 but < 90, and undefined if < 85 (since student will not pass). Done

//Create an object named classOf1A with a property named students which is an array containing all 4 student objects in it. Done

//Create a method for the object classOf1A named countHonorStudents() that will return the number of honor students. Done

//Create a method for the object classOf1A named honorsPercentage() that will return the % of honor students from the batch's total number of students. Done

//Create a method for the object classOf1A named retrieveHonorStudentInfo() that will return all honor students' emails and ave. grades as an array of objects.

//Create a method for the object classOf1A named sortHonorStudentsByGradeDesc() that will return all honor students' emails and ave. grades as an array of objects sorted in descending order based on their grade averages.

let studentOne = {
	name: "John",
	email: "john@mail.com",
	grades: [89, 84, 78, 88],
	login(){
		console.log(`${this.email} has logged in`)
	},
	logout(){
		console.log(`${this.email} has logged out`)
	},
	listGrades(){
		console.log(`${this.name}'s quarterly averages are: ${this.grades}`)
	},
	computeAve(){
		let sum = 0;
		this.grades.forEach((num) => {sum += num});
		let average = sum / this.grades.length;
		console.log(average)
	},
	willPass(){

		let sum = 0;
		this.grades.forEach((num) => {sum += num});
		let average = sum / this.grades.length;
		
		if(average >= 85){
			return true
		}else{
			return false
		}
	},
	willPassWithHonors(){

		let sum = 0;
		this.grades.forEach((num) => {sum += num});
		let average = sum / this.grades.length;
		
		if(average >= 90){
			return true
		}else if(average >= 85 &&
				 average < 90){
			return false
		}else{
			return undefined
		}
	}


}

let studentTwo = {
	name: "Joe",
	email: "joe@mail.com",
	grades: [78, 82, 79, 85],
	login(){
		console.log(`${this.email} has logged in`)
	},
	logout(){
		console.log(`${this.email} has logged out`)
	},
	listGrades(){
		console.log(`${this.name}'s quarterly averages are: ${this.grades}`)
	},
	computeAve(){
		let sum = 0;
		this.grades.forEach((num) => {sum += num});
		let average = sum / this.grades.length;
		console.log(average)
	},
	willPass(){

		let sum = 0;
		this.grades.forEach((num) => {sum += num});
		let average = sum / this.grades.length;
		
		if(average >= 85){
			return true
		}else{
			return false
		}

	},
	willPassWithHonors(){

		let sum = 0;
		this.grades.forEach((num) => {sum += num});
		let average = sum / this.grades.length;
		
		if(average >= 90){
			return true
		}else if(average >= 85 &&
				 average < 90){
			return false
		}else{
			return undefined
		}
	}

}

let studentThree = {
	name: "Jane",
	email: "jane@mail.com",
	grades: [87, 89, 91, 93],
	login(){
		console.log(`${this.email} has logged in`)
	},
	logout(){
		console.log(`${this.email} has logged out`)
	},
	listGrades(){
		console.log(`${this.name}'s quarterly averages are: ${this.grades}`)
	},
	computeAve(){
		let sum = 0;
		this.grades.forEach((num) => {sum += num});
		let average = sum / this.grades.length;
		console.log(average)
	},
	willPass(){

		let sum = 0;
		this.grades.forEach((num) => {sum += num});
		let average = sum / this.grades.length;
		
		if(average >= 85){
			return true
		}else{
			return false
		}

	},
	willPassWithHonors(){

		let sum = 0;
		this.grades.forEach((num) => {sum += num});
		let average = sum / this.grades.length;
		
		if(average >= 90){
			return true
		}else if(average >= 85 &&
				 average < 90){
			return false
		}else{
			return undefined
		}
	}
}

let studentFour = {
	name: "Jessie",
	email: "jessie@mail.com",
	grades: [91, 89, 92, 93],
	login(){
		console.log(`${this.email} has logged in`)
	},
	logout(){
		console.log(`${this.email} has logged out`)
	},
	listGrades(){
		console.log(`${this.name}'s quarterly averages are: ${this.grades}`)
	},
	computeAve(){
		let sum = 0;
		this.grades.forEach((num) => {sum += num});
		let average = sum / this.grades.length;
		console.log(average)
	},
	willPass(){

		let sum = 0;
		this.grades.forEach((num) => {sum += num});
		let average = sum / this.grades.length;
		
		if(average >= 85){
			return true
		}else{
			return false
		}

	},
	willPassWithHonors(){

		let sum = 0;
		this.grades.forEach((num) => {sum += num});
		let average = sum / this.grades.length;
		
		if(average >= 90){
			return true
		}else if(average >= 85 &&
				 average < 90){
			return false
		}else{
			return undefined
		}
	}
}

let classOf1A = {
	students: [studentOne, studentTwo, studentThree, studentFour],
	countHonorStudents(){

		let noOfHonorStudents = 0;

		for(let i = 0; i <= this.students.length-1; i++){

			if(this.students[i].willPassWithHonors() === true){
				noOfHonorStudents++
			}
		}
		console.log(noOfHonorStudents)
	},
	honorsPercentage(){

		let noOfHonorStudents = 0;

		for(let i = 0; i <= this.students.length-1; i++){

			if(this.students[i].willPassWithHonors() === true){
				noOfHonorStudents++
			}
		}
		
		let percentage = (noOfHonorStudents / this.students.length) * 100;

		console.log(`${percentage}%`)

	},
	retrieveHonorStudentInfo(){


	}


}
